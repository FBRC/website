# website

Website for the Flow Battery Research Collective at https://fbrc.dev. Made with [Quarto](https://quarto.org/).

## Contributing

Please see the [contributing guide](https://fbrc.dev/contributing.html) on our website!

## Community

Discussion about the project happens on the [FBRC forum](https://fbrc.nodebb.com/)!